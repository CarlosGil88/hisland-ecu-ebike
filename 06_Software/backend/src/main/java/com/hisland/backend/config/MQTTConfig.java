package com.hisland.backend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * this class is used to configure the connection
 */
@Component
public class MQTTConfig {

    @Value("${mqtt.retryinterval}")
    private int retryInterval;
    @Value("${mqtt.clientid}")
    private String clientId;
    @Value("${mqtt.host}")
    private String host;
    @Value("${mqtt.port}")
    private String port;
    @Value("${mqtt.password}")
    private String password;
    @Value("${mqtt.username}")
    private String username;
    @Value("${mqtt.portprotocol}")
    private String portProtocol;


    public MQTTConfig() {
    }

    public MQTTConfig(int retryInterval, String clientId, String host, String port, String password, String username, String portProtocol) {
        this.retryInterval = retryInterval;
        this.clientId = clientId;
        this.host = host;
        this.port = port;
        this.password = password;
        this.username = username;
        this.portProtocol = portProtocol;
    }

    public int getRetryInterval() {
        return retryInterval;
    }

    public void setRetryInterval(int retryInterval) {
        this.retryInterval = retryInterval;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


    public String getPortProtocol() {
        return portProtocol;
    }

    public void setPortProtocol(String portProtocol) {
        this.portProtocol = portProtocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
