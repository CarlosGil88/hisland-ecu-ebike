package com.hisland.backend.communication.processing;

import com.hisland.backend.communication.Notifier;
import com.hisland.backend.communication.receiver.MQTTReceiver;
import com.sun.xml.bind.v2.TODO;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * processes light data
 */
@Component("light/bool")
public class LightProcessor extends Notifier implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightProcessor.class);

    private final
    SimpMessagingTemplate template;

    private boolean light;



    @Autowired
    public LightProcessor(SimpMessagingTemplate template) {
        this.template = template;
    }


    /**
     * changes the status of light
     * @param MQTTMessageData
     */
    @Override
    public void detect(String MQTTMessageData) {
        LOGGER.info("LightProcessor IS ACTIVATED");

        if (MQTTMessageData.toLowerCase().equals("true")){
            light = true;
        }else {
            light = false;
        }
    }

    /**
     * returns light boolean
     * @return
     */
    public boolean getLight() {
        return light;
    }
}
