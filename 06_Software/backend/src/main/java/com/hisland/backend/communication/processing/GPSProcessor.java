package com.hisland.backend.communication.processing;
import com.hisland.backend.communication.Notifier;
import com.hisland.backend.communication.receiver.MQTTReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 *processes gps data
 */
@Component("gps/coordinates")
public class GPSProcessor extends Notifier implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(GPSProcessor.class);

    private final
    SimpMessagingTemplate template;


    @Autowired
    public GPSProcessor(SimpMessagingTemplate template) {
        this.template = template; }


    /**
     * sends MQTTMessageData to mqtt broker
     * @param JSONMessageGPSData
     */
    public void detect(String JSONMessageGPSData) {
        LOGGER.info("GPSProcessor IS ACTIVATED");
        template.convertAndSend("/Gps", JSONMessageGPSData);

    }


}

