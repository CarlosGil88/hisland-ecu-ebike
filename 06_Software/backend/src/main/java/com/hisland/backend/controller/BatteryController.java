package com.hisland.backend.controller;

import com.hisland.backend.exception.CommunicationException;
import com.hisland.backend.service.ProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
public class BatteryController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatteryController.class);
    private final ProcessService processService;


    public BatteryController(ProcessService processService) {
        this.processService = processService;
    }

    @PostMapping("/batterystatus")
    @ResponseStatus(HttpStatus.OK)
    public void batteryCall() throws CommunicationException {
        LOGGER.info("batterystatus call");
        processService.setBatteryProcessor();
    }

    @GetMapping("/batterystatus")
    @ResponseStatus(HttpStatus.OK)
    public void getBatteryStatus() throws CommunicationException {
        LOGGER.info("get call for batterystatus");
        processService.setBatteryProcessor();
    }
}
