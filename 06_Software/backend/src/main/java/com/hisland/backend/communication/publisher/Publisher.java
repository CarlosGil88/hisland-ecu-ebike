package com.hisland.backend.communication.publisher;

import com.hisland.backend.exception.CommunicationException;

public interface Publisher {
    void publish(String MQTTMessageData,String topic) throws CommunicationException;
}
