package com.hisland.backend;

import com.hisland.backend.communication.processing.BatteryProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableJpaRepositories
@EntityScan(basePackages = {"com.hisland.backend.model"})
public class BackendApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(OnStart.class);

    private final OnStart onStart;

    @Autowired
    public BackendApplication(OnStart onStart) {
        this.onStart = onStart;
    }


    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(BackendApplication.class);
        SpringApplication.run(BackendApplication.class, args);
    }

    @PostConstruct
    void setOnStart() {
        LOGGER.debug("BackendApplication's addInitHooks() called");
        onStart.start();
    }


}


