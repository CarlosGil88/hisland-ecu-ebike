package com.hisland.backend.service;

import com.hisland.backend.communication.processing.LightProcessor;
import com.hisland.backend.exception.CommunicationException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class ProcessServiceTest {

    @Mock
    ProcessService processService;
    @Mock
    LightProcessor lightProcessor;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void setLightProcessor() throws CommunicationException {
        processService.setLightProcessor("test");
        verify(processService, atLeastOnce()).setLightProcessor("test");
    }

    @Test
    public void setLightProcessor2() throws CommunicationException {
        processService.setLightProcessor("rrrr");
        verify(processService, times(0)).setLightProcessor("test");
    }

    @Test
    public void setPowerProcessor() throws CommunicationException {
        processService.setPowerProcessor("test");
        verify(processService, atLeastOnce()).setPowerProcessor("test");
    }
    @Test
    public void setPowerProcessor2() throws CommunicationException {
        processService.setPowerProcessor("rrrr");
        verify(processService, times(0)).setPowerProcessor("test");
    }
    @Test
    public void setBatteryProcessor() throws CommunicationException {
        processService.setBatteryProcessor();
        verify(processService, atLeastOnce()).setBatteryProcessor();
    }



}
