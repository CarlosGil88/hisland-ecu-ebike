package com.hisland.backend.communication.processing;

import com.hisland.backend.BackendApplication;
import com.hisland.backend.exception.MyException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BackendApplication.class)
@WebAppConfiguration
public class PowerProcessorTest {
        @Mock
        PowerProcessor powerProcessor;

    @Before
    public void setup() {
        when(powerProcessor.getPower()).thenReturn(true);

    }

    @Test
    public void detect() {
        powerProcessor.detect("test");
        verify(powerProcessor, times(1)).detect("test");

    }

    @Test
    public void getPower() {
        powerProcessor.detect("true");
        assertEquals(powerProcessor.getPower() , true);
    }
    @Test
    public void getPower2() {
        powerProcessor.detect("true");
        assertNotEquals(powerProcessor.getPower() , false);
    }

}
