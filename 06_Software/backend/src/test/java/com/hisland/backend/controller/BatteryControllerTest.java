package com.hisland.backend.controller;

import com.hisland.backend.BackendApplication;
import com.hisland.backend.communication.processing.BatteryProcessor;
import com.hisland.backend.service.ProcessService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BackendApplication.class)
@WebAppConfiguration
public class BatteryControllerTest {
    @Autowired
    private WebApplicationContext wac;
    @MockBean
    ProcessService processService;
    private MockMvc mockMvc;


    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    @Before
    @Transactional
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

    }

    @Test
    public void BasicSuccessBatteryCall() throws Exception {
        mockMvc.perform(post("/batterystatus")
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk());
    }

    @Test
    public void BasicSuccessBatteryCallAndMethodCheck() throws Exception {
        mockMvc.perform(post("/batterystatus").accept(CONTENT_TYPE));
        verify(processService, times(1)).setBatteryProcessor();
    }

    @Test
    public void BasicFAilBatteryCallAndMethodCheck() throws Exception {
        mockMvc.perform(post("/batterystatu").accept(CONTENT_TYPE));
        verify(processService, times(0)).setBatteryProcessor();
    }

    @Test
    public void BasicFailBatteryCall() throws Exception {
        mockMvc.perform(post("/batterystas")
                .accept(CONTENT_TYPE))
                .andExpect(status().isNotFound());
    }

   @Test
   public void BasicSuccessGetBatteryStatus() throws Exception {
       mockMvc.perform(get("/batterystatus")
               .accept(CONTENT_TYPE))
               .andExpect(status().isOk());
   }

   @Test
   public void BasicFailGetBatteryStatus() throws Exception {
       mockMvc.perform(get("/batterystus")
               .accept(CONTENT_TYPE))
               .andExpect(status().isNotFound());
   }

    @Test
    public void BasicSuccessGetBatteryStatusCallAndMethodCheck() throws Exception {
        mockMvc.perform(get("/batterystatus").accept(CONTENT_TYPE));
        verify(processService, times(1)).setBatteryProcessor();
    }

    @Test
    public void BasicfailGetBatteryStatusCallAndMethodCheck() throws Exception {
        mockMvc.perform(get("/batterystatu").accept(CONTENT_TYPE));
        verify(processService, times(0)).setBatteryProcessor();
    }

}
