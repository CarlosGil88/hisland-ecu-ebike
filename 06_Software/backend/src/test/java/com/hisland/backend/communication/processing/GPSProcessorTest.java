package com.hisland.backend.communication.processing;

import com.hisland.backend.BackendApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BackendApplication.class)
public class GPSProcessorTest {
    @Mock
    GPSProcessor gpsProcessor;

    @Test
    public void detect() {
        gpsProcessor.detect("test");
    }
}
