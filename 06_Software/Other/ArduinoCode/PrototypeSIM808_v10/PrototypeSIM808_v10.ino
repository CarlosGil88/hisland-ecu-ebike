/*


  SIM808 MQTT GPS  developed and CAN included
  Able to turn on/off the eBike from cloudMQTT
  Boards used:
  Arduino MEGA
  SIM808 shield from elecrow    for GPRS connection and GPS location

    Functions:
  - GPS data
  - Diagnostics
  - Turn on/off eBike
  - GPRS data to cloud
  - Smart print (Serialprint)
  - CAN reading bat level

  Due to a lack of memory space we couldn't add the CAN part with the Arduino UNO
  This sketch uses 20826 bytes (64%) of max 32kB. Global vars uses 73% of the dinamic memory, just 545bytes for local vars. max is 2048bytes
  **Now with the Arduino MEGA we don't have this problem of memory

  This sketch demonstrates the capabilities of the pubsub library in combination
  with the TinyGSM - SIM808 board/library.
  It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" at the begining of the conexion
    check it at cloud mqtt "websocket ui"
    https://www.cloudmqtt.com/
  - subscribes to the topics for reading and publishing "TestIn", "light/bool","power/bool","battery/call" and "curstate/call"
    check the messages sent from Arduino and received also at cloud mqtt "websocket ui"
    It assumes the received payloads are strings not binary
  - Example. If the first character of the topic "light/bool" is an 1, switch ON the Turning Right light,
    check the actions that will occur depending of the Topic and its payload at 'callback()' function

  It will reconnect to the server if the connection is lost using a blocking
  reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
  achieve the same result without blocking the main loop.

  Library & Documentation API about PubSubClient() :
  https://pubsubclient.knolleary.net/api.html
  Library & Documentation API about TinyGSM() :
  https://github.com/vshymanskyy/TinyGSM/tree/master/src
  No Library needed for GPS, all get by AT commands:
  https://cdn-shop.adafruit.com/datasheets/SIM808_GPS_Application_Note_V1.00.pdf

  the WebApp is developed here: http://jgecu.surge.sh/
  Still need review in the BE&FE part.

  MQTT GPRS connection web example: http://shahrulnizam.com/arduino-lesson-sim800-mqtt/


*/

/*------------- LIBRARIES Includes & Defines --------------*/
#define TINY_GSM_MODEM_SIM808
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <SPI.h>
#include "mcp_can.h"
//uncomment if using Arduino UNO and
//#include <SoftwareSerial.h>
//SoftwareSerial SerialAT(7, 8); //RX,TX
//uncomment if using Arduino MEGA
#define SerialAT Serial3
#define DEBUG true

/*------------- VARIABLES Declaration --------------*/
TinyGsm modem(SerialAT);
TinyGsmClient SIMclient(modem);
PubSubClient client(SIMclient); //id



//********************************* FOR Printing consuming less RAM ******************************//
/*
   We used this function with Arduino UNO,
   now with the MEGA we don't need it really,
   but is still here just in case
*/
int freeRam () {
  extern int __heap_start, *__brkval;
  int v;
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

void StreamPrint_progmem(Print &out, PGM_P format, ...)
{
  // program memory version of printf - copy of format string and result share a buffer
  // so as to avoid too much memory use
  char formatString[128], *ptr;
  strncpy_P( formatString, format, sizeof(formatString) ); // copy in from program mem
  // null terminate - leave last char since we might need it in worst case for result's \0
  formatString[ sizeof(formatString) - 2 ] = '\0';
  ptr = &formatString[ strlen(formatString) + 1 ]; // our result buffer...
  va_list args;
  va_start (args, format);
  vsnprintf(ptr, sizeof(formatString) - 1 - strlen(formatString), formatString, args );
  va_end (args);
  formatString[ sizeof(formatString) - 1 ] = '\0';
  out.print(ptr);
}

#define Serialprint(format, ...) StreamPrint_progmem(Serial,PSTR(format),##__VA_ARGS__)
#define Streamprint(stream,format, ...) StreamPrint_progmem(stream,PSTR(format),##__VA_ARGS__)
//************************************************************************************************************//



//Network details
const char apn[]  = "online.telia.se";
const char user[] = "";
const char pass[] = "";


//mqtt vars
const char* mqtt_server = "m24.cloudmqtt.com" ;         // at CloudMQTT web :"Your MQTT Server IP address";
const char* mqtt_user = "rxhwkrwu" ;//"ovkfmmrv";  ovkfmmrv                   // Your user at CloudMQTT
const char* mqtt_password = "euP2FEqY5IlV";//"b6Q5wTu-3-OR";             //  Your password at CloudMQTT
const  int port =  14889;//15139;                                //just Port, not the SSL

//for control vars
byte KEY = 23;      //red      //Power eBike on         //Switch RED - YELLOW
byte WHEEL = 25;    //blue     //Lock Wheel             //Alarm lock wheel
byte LIGHT = 27 ;   //green    //Power just Lights      //Switch RED - YELLOW  and PINK - AIR
byte IGNITION = 29; //orange   //Lock Motor             //Switch PINK- GND
byte T_LEFT = 5;   //  // Turning Light LEFT            //Switch GREY - ORANGE
byte T_RIGHT = 6;  //  // Turning Light RIGHT           //Switch GREY - BLUE
byte SIMPOWER = 9;

unsigned long previousMillis = 0;

//gps vars
char coord[40];
char lat[10];
char lon[10];
char pastcoord[40];
// gps vars SIM808
//String data[5];
String state, timegps, latitude, longitude;
String pastlatitude;

//batt vars
long unsigned int rxId;
unsigned long rcvTime;
unsigned char len = 0;
unsigned char buf[8];
unsigned int bat = 0;
const int SPI_CS_PIN = 53;  //53 for Arduino MEGA, pin 10 for UNO
MCP_CAN CAN(SPI_CS_PIN);  // Set CS pin

// sendLocationStatus and sendDiagnostic timers
uint32_t timer = millis();
uint32_t timerDiagnostics = millis();

//status and control vars of light and power
char* power = "false";
char* light = "false";

//diagnostic vars
char* breaks = "ok";
char* engine = "engine-paused health-ok";
char* ecu = "ecu-running-ok LiPo-bat-recharging";
char* batt = "f'healthd(0) battery l={batt_l} v={batt_v} t={batt_t} h=2 st=2'";


/*------------- FUNCTIONS Declaration --------------*/
void setup_wifi(void);
bool isTopic(char* topic, char* publishTopic);
char isPayload(byte* payload, char compchr);
void callback(char* topic, byte* payload, unsigned int length);
void setup_blink(void);
void sendLocationStatus(void);
void sendDiagnostics(void);
void sendData(String command, const int timeout, boolean debug);
void subscribingMQTT();
void powerSIM(void);
void setup_switch(void);
void setup_gps(void);
void setup_can(void);


/*--------------------- SET UP  ------------------------*/
void setup() {
  Serial.begin(9600);
  SerialAT.begin(9600);
  delay(2000);
  powerSIM();
  Serial.print("Powered on");
  setup_switch();
  delay(1000);
  //modem.restart();
  setup_wifi();
  Serial.print(" Wifi on ");
  client.setServer(mqtt_server, port); // server, Port (not SSL)
  client.setCallback(callback);
  delay(1000);
  setup_gps();
  Serial.println(" gps on ");
  delay(1000);
  setup_can();
  delay(100);
  Serial.println(" CAN on ");

}

void powerSIM() {
  sendData("AT+CPOWD=1", 3000, DEBUG);
  pinMode(SIMPOWER, OUTPUT);
  digitalWrite(SIMPOWER, HIGH);
  delay(2000);
  digitalWrite(SIMPOWER, LOW);
}

void setup_switch() {
  pinMode(IGNITION, OUTPUT);
  digitalWrite(IGNITION, LOW); // locked
  pinMode(LIGHT, OUTPUT);
  digitalWrite(LIGHT, LOW);  // not lighted
  pinMode(KEY, OUTPUT);
  digitalWrite(KEY, LOW);    // not powered
  pinMode(WHEEL, OUTPUT);
  digitalWrite(WHEEL, HIGH); // locked wheel
  pinMode(T_LEFT, OUTPUT);   /*TODO: put the lights like this and not with the single LIGHT pin*/
  pinMode(T_RIGHT, OUTPUT);
}
void setup_gps() {
  sendData("AT+CGPSPWR=1", 2000, DEBUG); // sets the GPS engine ON
  sendData("AT+CGPSINF=0", 1000, DEBUG); //  returns a single NMEA sentence of GPS.
  Serialprint("gps");
}


void setup_can(void) {
  while (CAN_OK != CAN.begin(CAN_250KBPS))              // init can bus : eBike baudrate = 250k 
  {
    Serialprint("CAN BUS Module Failed to Initialized");
    //Serial.println("Retrying....");
    delay(100);
  }
  Serialprint("CAN BUS Module Initialized!");
  Serial.println("Time\t\tPGN\t\tByte0\tByte1\tByte2\tByte3\tByte4\tByte5\tByte6\tByte7");
}

void setup_wifi() {
  Serialprint("System start.");
  Serial.println("Modem: " + modem.getModemInfo());
  Serial.println("Searching for telco provider.");
  if (!modem.waitForNetwork())
  {
    Serialprint("fail");
    while (true);
  }
  //Serial.println("Connected to telco.");
  Serial.println("Signal Quality: " + String(modem.getSignalQuality()));
  //Serial.println("Connecting to GPRS network.");
  if (!modem.gprsConnect(apn, user, pass))
  {
    Serial.println("fail");
    while (true);
  }
  //Serial.println("Connected to GPRS: " + String(apn));
  client.setServer(mqtt_server, port);
  client.setCallback(callback);
  //Serial.print("Connecting to MQTT Broker: " + String(mqtt_server));
  while (reconnect() == false) continue;
}


boolean reconnect() {
  if (!client.connect("MQTTEcuClient", mqtt_user, mqtt_password))
  {
    Serialprint(".");
    return false;
  }
  Serialprint("Connected to broker.");
  subscribingMQTT();
  //client.subscribe(topicIn);
  return client.connected();
}

void subscribingMQTT() {
  client.subscribe("TestIn"); // for debugging
  client.subscribe("light/bool");
  client.subscribe("power/bool");
  client.subscribe("battery/call");
  client.subscribe("curstate/call");
}

//fo show
void setup_blink() { // Function as Alarm or Find my eBike, It blinks the turning lights and blocks the motor
  unsigned long currentMillis = millis();
  unsigned long previousMillis = 0;
  Serial.println(" * blinking lights * ");
  digitalWrite(KEY, HIGH);     // ON POWER
  digitalWrite(IGNITION, LOW); //we want to ensure the bike canNOT move
  digitalWrite(WHEEL, HIGH);   //locked wheel
  digitalWrite(LIGHT, HIGH);
  for (int i = 0; i < 15; i++) {
    digitalWrite(LIGHT, LOW);
    delay(300);
    digitalWrite(LIGHT, HIGH);
    delay(300);
  }
  Serialprint(" end blinking ");
  delay(5000);
  digitalWrite(LIGHT, LOW);
  digitalWrite(KEY, LOW);  // OFF POWER
}


//utility methods
bool isTopic(char* topic, char* publishTopic) {
  return (strcmp(topic, publishTopic) == 0);
}

char isPayload(byte* payload, char compchr) {
  return ((char)payload[0] == compchr);
}

//_______________________________________ MQTT CALL BACKs _____________________________________
//_____________________________________________________________________________________________
void callback(char* topic, byte* payload, unsigned int length) {
  // go to CloudMQTT WEBSOCKET UI to send messages
  //Serial.print("Message arrived [");
  //Serial.print(topic);
  //Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.println((char)payload[i]);
  }

  if (isTopic(topic, "light/bool")) {
    if (isPayload(payload, '0')) {
      digitalWrite(LIGHT, LOW);   // OFF Turning Lights
      light = "false";
      client.publish("ecu/debug/light/status", light);
    } else if (isPayload(payload, '1')) {
      digitalWrite(LIGHT, HIGH);  // ON Turning Lights
      light = "true";
      client.publish("ecu/debug/light/status", light);
    }

  } else if (isTopic(topic, "power/bool")) {
    if (isPayload(payload, '0')) {
      digitalWrite(KEY, LOW);  // OFF POWER
      digitalWrite(WHEEL, HIGH); // wheel locked
      power = "false";
      client.publish("ecu/debug/power/status", power);
    } else if (isPayload(payload, '1')) {
      digitalWrite(KEY, HIGH);  // ON POWER
      digitalWrite(IGNITION, LOW); //we want to ensure the bike can move
      digitalWrite(WHEEL, LOW);
      power = "true";
      client.publish("ecu/debug/power/status", power);
    }

  } else if (isTopic(topic, "TestIn")) { //DEBUG & Test
    if (isPayload(payload, 'b')) {
      //Serial.println("TestIn b - batt stat");
    } else if (isPayload(payload, 'k')) {
      setup_blink();  // for debug
      //Serial.println("TestIn k - blink");
    }

  } else if (isTopic(topic, "battery/call")) {
    client.publish("battery/json", "{\"id\":0,\"cycle\":\"85\",\"status\":\"46\" }");

  } else if (isTopic(topic, "curstate/call")) {
    /*
       curstate aimed to give info to calculate with google maps
       how far could you arrive with the battery level, or
       how much consumption to your destination will you have,
       it's more a BackEnd feature
    */
    client.publish("curstate/json", "data");

  } else {
    //errorMessage
    client.publish("inTopic/error", "error: topic doesn't match any callback topic");
  }
}
//________________________________________________________________________________________________________
//________________________________________________________________________________________________________


//utility methods II
void sendTabData(String command , const int timeout , boolean debug) {

  String data[5] = "";
  SerialAT.println(command);
  long int time = millis();
  int i = 0;

  while ((time + timeout) > millis()) {
    while (SerialAT.available()) {
      char c = SerialAT.read();
      if (c != ',') {
        data[i] += c;
        //delay(100);
      } else {
        i++;
      }
      if (i == 5) {
        //delay(100);
        goto exitL;
      }
    }
} exitL:
  if (debug) {
    state = data[1];
    timegps = data[2];
    pastlatitude = latitude;
    latitude = data[3];
    longitude = data[4];
  }
}

void sendData(String command, const int timeout, boolean debug) {
  String response = "";
  SerialAT.println(command);
  delay(5);
  if (debug) {
    long int time = millis();
    while ( (time + timeout) > millis()) {
      while (SerialAT.available()) {
        response += char(SerialAT.read());
      }
    }
    // Serial.print(response);
  }
}


/*****************************************************************************/
/***************************    LOOP    **************************************/
/*****************************************************************************/
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  sendLocationStatus();
  sendDiagnostics();
  sendBatteryStatus();
}

//loopy bits
void sendDiagnostics() {
  if (timerDiagnostics > millis())  timerDiagnostics = millis();
  if (millis() - timerDiagnostics > 500) {
    timerDiagnostics = millis(); // reset the
    Serial.println("Diagnostics: ");
    //Serial.println("{\"id\":0,\"battery\":\"ok\",\"wheels\":\"ok\", \"lights\": [{\"front\": \"ok\"},{\"back\": \"ok\"},{\"other\": \"ok\"} ], \"suspention\": \"ok\", \"breaks\": \"ok\" }");
    //client.publish("Diagnostics","{\"id\":0,\"battery\":\"ok\",\"breaks\":\"ok\", \"lights\"[{\"front\": \"ok\"},{\"back\": \"ok\"},{\"suspention\": \"ok\"} ]");
    if (power == "true") {
      client.publish("diagnostics/breaks/string", breaks);
      client.publish("diagnostics/engine/string", engine);
      client.publish("diagnostics/ecu/string", ecu);
      client.publish("diagnostics/battery/string", batt);
    }else{
      client.publish("diagnostics/string", "bike is off");
      }
  }
}

void sendLocationStatus() {

  //sendData("AT+CIPGSMLOC=1,1",3000,DEBUG);
  sendTabData("AT+CGNSINF", 500, DEBUG);
  Serial.println(" asking gps ");
  //sendData( "AT+CGNSINF",1000,DEBUG);   // returns a single NMEA sentence of GPS.
  if (state != 0) {
    //    Serial.println("State  :"+state);
    //    Serial.println("Time  :"+timegps);
    //    Serial.println("Latitude  :" + latitude);
    //    Serial.println("Longitude  :" + longitude);
    SerialAT.print(latitude);
    SerialAT.print(",");
    SerialAT.print (longitude);

    latitude.toCharArray(lat, 10);
    longitude.toCharArray(lon, 10);
    sprintf (coord, "{\"id\":0,\"longitude\":%s,\"latitude\":%s}", lon, lat);
    //   Serial.print("Publish message: ");
    //   Serial.println(coord);
    if (latitude[2] != pastlatitude[2]) {
      Serial.println("Latitude  :" + latitude);
      Serial.println("Longitude  :" + longitude);
      client.publish("gps/coordinates", coord);
    }
    SerialAT.println((char)26); // End AT command with a ^Z, ASCII code 26
    delay(100);
    SerialAT.flush();
  }else {
    Serial.println("GPS Initialising...");
  }

}

void sendBatteryStatus() {
  Serial.println(" asking batt CAN ");
  char battery[3];
  if (power == "true") {
    //setup_can();
    if (CAN_MSGAVAIL == CAN.checkReceive()) {          // check if data coming
      Serialprint("received");
      rcvTime = millis();
      CAN.readMsgBuf(&len, buf);    // read data,  len: data length, buf: data buf
      rxId = CAN.getCanId();
      Serial.print(rcvTime);
      Serial.print("\t\t");
      Serial.print("0x");
      Serial.print(rxId);
      Serial.print(rxId, HEX);
      Serial.print("\t");
      for (int i = 0; i < len; i++) // print the data
      {
        if (buf[i] > 8) {
          Serial.print("0x");
          Serial.print(buf[i], HEX);
        }
        else {
          Serial.print("0x0");
          Serial.print(buf[i], HEX);
        }

        //Serial.print("0x");
        //Serial.print(buf[i], HEX);
        Serial.print("\t");
      }
      Serial.println();
      if (rxId == "523") {    // Received Message ID:0x20B  (is the one with the battery level status)
        //bat = buf[2];
        Serial.println("bat: ");
        Serial.print(buf[2], HEX);
        Serial.println(buf[2]);
      }
      sprintf(battery, "battery level: %u", buf[2]);
      client.publish("battery/json", battery);
    } else {
      Serial.print("I can't Read CAN");
    }/*end if CAN_MSGAVAIL*/
  } else {
    //Serialprint("*");
  }
}




