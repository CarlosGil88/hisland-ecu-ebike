#include <Adafruit_GPS.h>

/*
 Basic ESP8266 MQTT example
 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.
 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off
 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.
 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

Library & Documentation API about PubSubClient() :
https://pubsubclient.knolleary.net/api.html

 
*/

/*------------- LIBRARIES Includes & Defines --------------*/
#include <Adafruit_GPS.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(14,12); //RX,TX
Adafruit_GPS GPS(&mySerial);
#define GPSECHO  true


/*------------- VARIABLES Declaration --------------*/

const char* ssid = "Hisland";                           //"Your WiFi SSID";
const char* password = "W#tofigo82fa1He";               //"Your WiFi password";

const char* mqtt_server = "m24.cloudmqtt.com" ;         // at CloudMQTT web :"Your MQTT Server IP address";
const char* mqtt_user = "cwwyilxh" ;//"ovkfmmrv";                     // Your user at CloudMQTT
const char* mqtt_password = "ch3eicTMfASo";//"b6Q5wTu-3-OR";             //  Your password at CloudMQTT
const  int port = 12203 ;//15139;                                //just Port, not the SSL

int LED = D2;   // WHITE LED - Light          // WeMos pin D2
int POWER = D1 ;// ORANGE LED                 // WeMos pin D8
int BAT = A0;   // reads battery High or Low  // WeMos pin A0
int BAT_RED = 0 ;// RED LED                   // WeMos pin D3 
int BAT_GR = 2;  // GREEN LED                 // WeMos pin D4 

WiFiClient espClient;
PubSubClient client(espClient); //id
long lastMsg = 0;
char msg[50];
int value = 0;
const char* works = "working";
char coord[60]; 
char data[40];
char latitude[20];
char longitude[20];
char altitude[20];
bool power = false ;
bool light = false ;
unsigned long previousMillis = 0; 
const long interval = 1000;  

/*------------- FUNCTIONS Declaration --------------*/
void setup_wifi(void);
void callback(char* topic, byte* payload, unsigned int length);
void errorMessage(void);
void batteryStatus(void);
void buzzer(void);
void setup_blink(void);


/*--------------------- SET UP  ------------------------*/
void setup() {
  
  setup_blink();
  Serial.begin(115200);  
  delay(5000);
  Serial.setDebugOutput(true);
  setup_wifi();
  client.setServer(mqtt_server,port);// server, Port (not SSL)
  client.setCallback(callback);
  
  GPS.begin(9600);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA);

  delay(1000);
  // Ask for firmware version
  mySerial.println(PMTK_Q_RELEASE);

}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  // go to CloudMQTT WEBSOCKET UI to send messages
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  // Switch on the LED if an 1 was received as first character
  if(strcmp(topic,"light/bool")==0){
        if ((char)payload[0] == '0') {
            digitalWrite(LED, HIGH);   // OFF LED
            client.publish("Message/recieved","true");
        } else if ( (char)payload[0] == '1' ){
            digitalWrite(LED, LOW);  // ON LED
            //client.publish("Light","on"); 
        }
  }else if(strcmp(topic,"power/bool")==0){
          if ( (char)payload[0] == '0' ){
          buzzer();
          digitalWrite(POWER, HIGH);  // OFF POWER
          client.publish("Power","off"); 
      } else if ( (char)payload[0] == '1' ){
          digitalWrite(POWER, LOW);  // ON POWER
          buzzer();//blinks Led
          digitalWrite(POWER, LOW);  // ON POWER
          client.publish("Power","on"); 
      } 
  }else if(strcmp(topic,"inTopic")==0){
      if ( (char)payload[0] == 'b' ){
          batteryStatus();  // Read Battery Status
      }else if ( (char)payload[0] == 'k' ){
          setup_blink();  // for debug
      }
  }else if(strcmp(topic,"diagnostics/json")==0){
      Serial.println("Diagnostics: ");
      Serial.println("{\"id\":0,\"battery\":\"ok\",\"wheels\":\"ok\", \"lights\": [{\"front\": \"ok\"},{\"back\": \"ok\"},{\"other\": \"ok\"} ], \"suspention\": \"ok\", \"breaks\": \"ok\" }");
      client.publish("Diagnostics","{\"id\":0,\"battery\":\"ok\",\"breaks\":\"ok\", \"lights\"[{\"front\": \"ok\"},{\"back\": \"ok\"},{\"suspention\": \"ok\"} ]");
  }else if(strcmp(topic,"battery/call")==0){
          //batteryStatus();  // Read Battery Status
          client.publish("battery/json","{\"id\":0,\"cycle\":\"85\",\"status\":\"46\" }");
  }else if(strcmp(topic,"curstate/call")==0){
          dtostrf(GPS.latitude, 6,2, latitude);
          dtostrf(GPS.longitude, 6,2, longitude);
          dtostrf(GPS.altitude, 6,2, altitude); 
          sprintf(data, "{\"id\":0,\"lat\":%s%s,\"lng\":%s%s,\"alt\":%s,\"bat\":%s}", &GPS.lat, latitude, &GPS.lon, longitude, altitude,"98"); //98 is a random num for batt %
          client.publish("curstate/json",data);
         
  }else {
        //  errorMessage();
      }

}

void errorMessage(){
      client.publish("inTopic/error","error"); 
  }

void setup_blink(){ // Make all the LEDs blink to be sure the connections are good and debugg
  pinMode(BUILTIN_LED, OUTPUT);     
  pinMode(LED, OUTPUT);
  digitalWrite(LED,LOW);
  delay(500);
  digitalWrite(LED,HIGH);
  pinMode(POWER, OUTPUT);
  digitalWrite(POWER,LOW);
  delay(500);
  digitalWrite(POWER,HIGH);
  pinMode(BAT, INPUT);
  pinMode(BAT_GR, OUTPUT);
  digitalWrite(BAT_GR,LOW);
  delay(500);
  digitalWrite(BAT_GR,HIGH);
  pinMode(BAT_RED, OUTPUT);
  digitalWrite(BAT_RED,LOW);
  delay(500);
  digitalWrite(BAT_RED,HIGH);
  buzzer();
  }

void batteryStatus(){ //It reads the pin A0- connect the wire to 5V or 3.3V (will be High status)or GND(will be Low status)
   int batValue = 0;
   batValue = analogRead(BAT);
   digitalWrite(BAT_GR,HIGH);
   digitalWrite(BAT_RED,HIGH);
   if( batValue < 20 ){
      Serial.print("Publish message: ");
      Serial.println(batValue);  
      client.publish("outTopic/BatteryStatus","Low"); 
      digitalWrite(BAT_RED,LOW);
    } else if( batValue > 150 ){
      Serial.print("Publish message: ");
      Serial.println(batValue);  
      client.publish("outTopic/BatteryStatus","High");
      digitalWrite(BAT_GR,LOW); 
    } else {
      client.publish("outTopic/BatteryStatus","Normal"); 
      digitalWrite(BAT_GR,HIGH);
      digitalWrite(BAT_RED,HIGH);
    }  
}

void buzzer(){ // blinks the orange LED 
        digitalWrite(POWER, LOW);
        delay(100);
        digitalWrite(POWER, HIGH); 
        delay(100);
        digitalWrite(POWER, LOW);
        delay(100);
        digitalWrite(POWER, HIGH); 
        delay(100);
}

uint32_t timer = millis();
void locationStatus(){
   char c = GPS.read();
  if ((c) && (GPSECHO))
    Serial.write(c);
  if (GPS.newNMEAreceived()) {
    if (!GPS.parse(GPS.lastNMEA()))  
      return;  
   }
  
  // if millis() or timer wraps around, we'll just reset it
  if (timer > millis())  timer = millis();

  // approximately every 2 seconds or so, print out the current stats
  if (millis() - timer > 2000) {
    timer = millis(); // reset the timer
    if (GPS.fix) {
      Serial.print("Location: ");
      Serial.print(GPS.latitude, 4); Serial.print(GPS.lat);
      Serial.print(", ");
      Serial.print(GPS.longitude, 4); Serial.println(GPS.lon);

      Serial.print("Speed (knots): "); Serial.println(GPS.speed);
      Serial.print("Angle: "); Serial.println(GPS.angle);
      Serial.print("Altitude: "); Serial.println(GPS.altitude);
      Serial.print("Satellites: "); Serial.println((int)GPS.satellites);
    }
    
    dtostrf(GPS.latitude, 6,2, latitude);
    dtostrf(GPS.longitude, 6,2, longitude);
    //sprintf (coord, "{\"id\":0,\"longitude\":%s%s,\"latitude\":%s%s}", &GPS.lat, latitude, &GPS.lon, longitude); 
    //sprintf (coord,"%s%s, %s%s", &GPS.lat, "25", &GPS.lon, "52"); 
    sprintf (coord,"{\"id\":0,\"longitude\":%s,\"latitude\":%s}", latitude,  longitude); 

    Serial.print("Publish message: ");
    Serial.println(coord);    //coord is the message sent to CloudMQTT, you can check at WEBSCOKET UI the message
    client.publish("gps/coordinates",coord); 
  }
}
  

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("erere", mqtt_user, mqtt_password)) { //id, user, passw
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world init");
      // ... and resubscribe
      client.subscribe("TestIn"); // SUBSCRIBE to "inTopic"
      client.subscribe("light/bool");
      //client.subscribe("power/be/char");
      //client.publish("light/status/bool")
      client.subscribe("gps/coordinates");
      //client.subscribe("diagnostics/json");
      client.subscribe("battery/call");
      //client.subscribe("curstate/call");
      } if (client.connect("ESP8266Client")) {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}




/*****************************************************************************/
/***************************    LOOP    **************************************/
/*****************************************************************************/
void loop() {
  
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  locationStatus();
}
