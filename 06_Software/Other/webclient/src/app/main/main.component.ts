import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  //styleUrls: ['./main.component.css']

})
export class MainComponent implements OnInit {


  constructor(private http: HttpClient) { }


  ngOnInit() {
  }

  test(){
    // Simple POST request :
   const _this = this;
   console.log("runs");
    this.http.post("http://localhost:9090/postLight", "bool=true");
  }

}
