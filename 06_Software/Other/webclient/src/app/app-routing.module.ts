import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AgmCoreModule} from '@agm/core';

const routes: Routes = [

];

@NgModule({
  imports: [RouterModule.forRoot(routes)/*,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB_riTMzMDmvB7XMfT6dbDH9bGzyPgOAJ4'
    })*/
  ],
  exports: [RouterModule],

})
export class AppRoutingModule {
}
