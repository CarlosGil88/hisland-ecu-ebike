import {Component, OnInit, ViewChild, NgZone} from '@angular/core';
import {AgmMap, GoogleMapsAPIWrapper, MapsAPILoader} from '@agm/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import $ from 'jquery';

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
}

interface gpsDTOJSON {
  id: string;
  longitude: number;
  latitude
}

/*let obj: gpsDTOJSON = JSON.parse('{ "myString": "string", "myNumber": 4 }');
console.log(obj.myString);
console.log(obj.myNumber);*/



@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {


  title = 'the ecu webclient';
  private serverUrl = 'http://localhost:9090/socket';
  private stompClient;


  hislandLat: any = 57.710518;
  hislandLng: any = 11.945343;
  n1: any = 0.001;
  @ViewChild(AgmMap) map: AgmMap;

  public location: Location = {
    lat: /*this.hislandLat*/57.7108933,
    lng: /*this.hislandLng*/11.9454818,
    marker: {
      lat: /*this.hislandLat*/57.7108933,
      lng: /*this.hislandLng*/11.9454818,
      draggable: false,
      label: 'e-bike'
    },
    zoom: 12
  };


  constructor(public mapsApiLoader: MapsAPILoader,
              private zone: NgZone,
              private wrapper: GoogleMapsAPIWrapper) {
    this.mapsApiLoader = mapsApiLoader;
    this.zone = zone;
    this.wrapper = wrapper;
    this.initializeWebSocketConnection();

  }

  private updateOnMap(latitude: number, longitude: number) {

    this.location.lat = latitude;
    this.location.marker.lat = latitude;

    this.location.lng = longitude;
    this.location.marker.lng = longitude;

    this.location.marker.draggable = false;
    this.map.fullscreenControl = false;
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/ecu', (message) => {
        //console.log('message: ' + message.toString());
        //console.log('body: ' + message.body);
        let obj: gpsDTOJSON = JSON.parse(message.body);
        console.log(obj.latitude + " - " + obj.longitude);
        console.log(new Date());
        that.updateOnMap(obj.latitude, obj.longitude);
      });
    });
  }



  ngOnInit() {

  }
}

