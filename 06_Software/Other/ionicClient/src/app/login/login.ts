import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',

})
export class LoginPage {

username: string;
password: string;

usernameVerified: string = 'foo';
passwordVerified: string = 'bar';


  constructor(    private router: Router,  private navController: NavController  ) {
  }

  login() {
    console.log("Username: " + this.username);
    console.log("Password: " + this.password);
    if (this.username == this.usernameVerified && this.password == this.passwordVerified) {
    console.log("!!!");
   // this.navController.navigateRoot('./vehicle-list')
    }
  }

}
