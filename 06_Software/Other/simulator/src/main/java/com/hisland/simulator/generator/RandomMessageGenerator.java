package com.hisland.simulator.generator;


import com.hisland.simulator.messengers.Messenger;
import com.hisland.simulator.model.GPSCoordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Component
@ConditionalOnProperty(name = "generator.type", havingValue = "random")
public class RandomMessageGenerator implements MessageGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(RandomMessageGenerator.class);
    private Random rand = new Random();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    @Value("${spits.begin}")
    private Integer begin;
    @Value("${spits.einde}")
    private Integer einde;
    int counter = 0;
    //private double latitude = 11.945343;


    private double generateLat(double latitudeBefore) {
        //latitudeBefore =+ 0.0002;
        double val = 0.0002;
        counter++;
        return latitudeBefore + val * counter;
    }

    @Override
    public GPSCoordinate generateMessage() {
        //57.710518, 11.945343
        return new GPSCoordinate(0, 57.710518, generateLat(11.945343));

    }

    public int getDelay() {
        int nu = LocalDateTime.now().getHour();
        int delay = (int) (Math.random() * (700 - 100));
        if (nu >= begin && nu <= einde) {
            delay = (int) (Math.random() * (900 - 400));
            LOGGER.info("Spits tijd");
            return delay;
        }
        return delay;
    }


    //@Override
    public void sendMessage(GPSCoordinate message) {
        LOGGER.info("SENT" + message.toString());

    }
}
