package com.hisland.simulator.generator;

import com.hisland.simulator.model.GPSCoordinate;
import org.springframework.stereotype.Component;

@Component
public interface MessageGenerator {
    GPSCoordinate generateMessage();
    int getDelay();
}
