package com.hisland.simulator.config;

import org.springframework.stereotype.Component;

@Component
public class MQTTConfig {
    int retryInterval;
    String clientId;
    String accessToken;
    String host;
    String port;
    String password;
    String username;


    public MQTTConfig() {
    }

    public MQTTConfig(int retryInterval, String clientId, String accessToken, String host, String port, String password, String username) {
        this.retryInterval = retryInterval;
        this.clientId = clientId;
        this.accessToken = accessToken;
        this.host = host;
        this.port = port;
        this.password = password;
        this.username = username;
    }

    public int getRetryInterval() {
        return retryInterval;
    }

    public void setRetryInterval(int retryInterval) {
        this.retryInterval = retryInterval;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
