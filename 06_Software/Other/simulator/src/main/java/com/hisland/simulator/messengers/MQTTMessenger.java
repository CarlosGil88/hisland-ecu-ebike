package com.hisland.simulator.messengers;

import com.hisland.simulator.config.MQTTConfig;
import com.hisland.simulator.config.MQTTinitializer;
import com.hisland.simulator.model.GPSCoordinate;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.UUID;
import java.util.concurrent.Callable;

@Component
@ConditionalOnProperty(name = "messenger.type", havingValue = "mqtt")
public class MQTTMessenger implements Callable<Void>, Messenger {
    private static final Logger LOGGER = LoggerFactory.getLogger(MQTTMessenger.class);
    public static final String TOPIC = "gps/coordinates";


    @Autowired
    MQTTinitializer mqtTinitializer;

    @Autowired
    MQTTConfig mqttConfig;

    private IMqttClient mqttClient;

    MqttMessage msg;

    int counter = 0;

    @Override
    public Void call() throws Exception {


        if (counter== 0){
            mqttClient = mqtTinitializer.init(new MQTTConfig(20, UUID.randomUUID().toString(), "ECU", "m24.cloudmqtt.com",  "12203", "ch3eicTMfASo", "cwwyilxh"));
            //mqttClient = mqtTinitializer.init(new MQTTConfig(100, "ECU", "ECU", "m24.cloudmqtt.com",  "15139", "b6Q5wTu-3-OR", "ovkfmmrv"));
       counter++;
        }
        if ( !mqttClient.isConnected()) {

            LOGGER.info("[I31] Client not connected.");
            return null;
        }
        //msg  = new MqttMessage("hello".getBytes());
        msg.setQos(2);
        msg.setRetained(true);
        mqttClient.publish(TOPIC,msg);

        //System.out.println("call");
        return null;

    }


    @Override
    public void sendMessage(GPSCoordinate message) {
        try {
            // msg  = new MqttMessage("hello".getBytes());

            msg  = new MqttMessage(message.toString().getBytes());
            System.out.println(message);
            this.call();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
