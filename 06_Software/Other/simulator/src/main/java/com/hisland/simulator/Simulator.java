package com.hisland.simulator;


import com.hisland.simulator.generator.MessageGenerator;
import com.hisland.simulator.messengers.Messenger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Simulator {
    private static final Logger LOGGER = LoggerFactory.getLogger(Simulator.class);

    private final MessageGenerator messageGenerator;
    private final Messenger messenger;
    private int counter;


    @Autowired
    public Simulator(MessageGenerator messageGenerator, Messenger messenger) {
        this.messageGenerator = messageGenerator;
        this.messenger = messenger;
    }

    @Autowired
    public void simulate() {
        do {
            try {
                messenger.sendMessage(messageGenerator.generateMessage());
                Thread.sleep(1000);
                counter++;


            } catch (InterruptedException e) {
                LOGGER.info("sleep");
                Thread.currentThread().interrupt();
            }
        //} while (messageGenerator.getDelay()>=0);
        } while (true);
    }
}
