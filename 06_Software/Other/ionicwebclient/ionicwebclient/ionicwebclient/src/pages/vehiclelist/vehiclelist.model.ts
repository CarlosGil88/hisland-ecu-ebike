export interface Evehicle {
  id: string;
  title: string;
  imageUrl: string;
  batterystatus: number;
  powerstatus: boolean;
}
