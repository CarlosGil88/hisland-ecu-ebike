import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
@Injectable({
    providedIn: "root"
})
export class Globals {

    socket: SockJS;
    stompClient: Stomp;
    //serverUrl: string = 'http://localhost:9090/';
    serverUrl: string = 'https://rocky-stream-10783.herokuapp.com';
    socketServerUrl: string = this.serverUrl + '/socket';

    constructor() {
        this.socket = new SockJS(this.socketServerUrl);
        this.stompClient = Stomp.over(this.socket);
        this.stompClient.connect();
    }

    initializeWebSocketConnection(callback?: Function) {
        if (!this.stompClient.connected) {
            this.stompClient.connect({}, function () {
                if (callback) {
                    callback();
                }
            });
        } else {
            if (callback) {
                callback();
            }
        }
    }
}
