import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DiagnosticsdetailPage } from './diagnosticsdetail.page';

const routes: Routes = [
  {
    path: '',
    component: DiagnosticsdetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DiagnosticsdetailPage]
})
export class DiagnosticsdetailPageModule {}
