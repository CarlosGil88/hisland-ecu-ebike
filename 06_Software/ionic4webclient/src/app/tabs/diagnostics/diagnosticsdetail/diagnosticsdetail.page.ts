import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DiagnosticsService } from '../diagnostics.service';
import { Diagnostic } from '../diagnostics.model';
import { NavController } from '@ionic/angular';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Globals } from 'src/app/globals';

@Component({
  selector: 'app-diagnosticsdetail',
  templateUrl: './diagnosticsdetail.page.html',
  styleUrls: ['./diagnosticsdetail.page.scss'],
})
export class DiagnosticsdetailPage implements OnInit {

  diagnosticDetail: Diagnostic;
  diagnostics: Array<{ detail: string }> = [];
  wsIsConnectedView: string;
  connectedColor = "";

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private diagnosticsService: DiagnosticsService,
    private globals: Globals) { }


  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('diagnosticsId')) {
        this.navCtrl.navigateBack('/controlcenter/tabs/diagnostics');
        return
      }
      this.diagnosticDetail = this.diagnosticsService.getDiagnosticDetail(paramMap.get('diagnosticsId'))
    });
  }

  ionViewDidLeave() {
    const that = this;
    that.globals.stompClient.unsubscribe('/' + this.diagnosticDetail.name);
    this.diagnostics.length = 0;
  }

  ionViewDidEnter() {
    this.checkConnection()
    this.initializeWebSocketConnection();
  }

  async initializeWebSocketConnection() {
    const _this = this;
    _this.connectedColor = "warning"
    _this.wsIsConnectedView = "websocket is connecting"
    this.globals.initializeWebSocketConnection(function () {
      _this.globals.stompClient.subscribe('/' + _this.diagnosticDetail.name, (message) => {
        console.log('message: ' + message.toString());
        console.log('body: ' + message.body);
        _this.checkConnection()
        _this.diagnostics.push({ detail: message.body })
        if (_this.diagnostics.length > 20) {
          _this.diagnostics.shift()
        }
      });
      _this.checkConnection()
    })
  }

  checkConnection() {
    if (this.globals.stompClient == undefined || !this.globals.stompClient.connected) {
      this.connectedColor = "warning"
      this.wsIsConnectedView = "connecting..."
    } else {
      this.connectedColor = "success"
      this.wsIsConnectedView = "connected"
    }
  }



}
