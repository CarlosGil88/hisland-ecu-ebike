import { Component, OnInit } from "@angular/core";
import { AlertController, LoadingController } from "@ionic/angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { TabsService } from '../tabs.service';
import { ControlsService } from './controls.service';

@Component({
  selector: "app-controls",
  templateUrl: "./controls.page.html",
  styleUrls: ["./controls.page.scss"]
})

export class ControlsPage {
  isPowered: boolean;
  isLit: boolean;

  constructor(private alertCtrl: AlertController,
    private http: HttpClient,
    private loadingController: LoadingController,
    private tabsService: TabsService,
    private controlsService: ControlsService) { }

  ionViewWillEnter() {
    this.isPowered = this.tabsService.power();
    this.isLit = this.tabsService.light();
  }

  togglePower() {
    this.presentLoading();
    this.controlsService.callTogglePower();
  }

  toggleLight() {
    this.presentLoading();
    this.controlsService.callToggleLight();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait',
      duration: 1000
    });
    await loading.present();
    const { role, data } = await loading.onDidDismiss();
  }
}
