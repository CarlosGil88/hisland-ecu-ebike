import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TabsService } from '../tabs.service';
import { Globals } from 'src/app/globals';

@Injectable({
  providedIn: 'root'
})
export class ControlsService {

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(
    private http: HttpClient,
    private tabservice: TabsService,
    private globals: Globals) { }

  callToggleLight() {
    if (this.tabservice.light()) {
      this.http
        .post<String>(this.globals.serverUrl + "/light", "1", this.httpOptions)
        .subscribe(data => {
          this.tabservice.setLight(false);
        });
    } else {
      this.http
        .post<String>(this.globals.serverUrl + "/light", "0", this.httpOptions)
        .subscribe(data => {
          this.tabservice.setLight(true);
        });
    }
  }

  callTogglePower() {
    if (this.tabservice.power()) {
      this.http
        .post<String>(this.globals.serverUrl + "/power", "1", this.httpOptions)
        .subscribe(data => {
          this.tabservice.setPower(false)
        });
    }
    else {
      this.http
        .post<String>(this.globals.serverUrl + "/power", "0", this.httpOptions)
        .subscribe(data => {
          this.tabservice.setPower(true)

        });
    }
  }
}
