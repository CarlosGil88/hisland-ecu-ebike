import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';
var routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'map', children: [
                    {
                        path: '', loadChildren: './map/map.module#MapPageModule'
                    }
                ]
            },
            {
                path: 'controls', children: [
                    {
                        path: '', loadChildren: './controls/controls.module#ControlsPageModule'
                    }
                ]
            },
            {
                path: 'diagnostics', children: [
                    {
                        path: '', loadChildren: './diagnostics/diagnostics.module#DiagnosticsPageModule'
                    }
                ]
            },
            {
                path: 'details', children: [
                    {
                        path: '', loadChildren: './details/details.module#DetailsPageModule'
                    }
                ]
            },
            {
                path: 'user', children: [
                    {
                        path: '', loadChildren: './usersettings/usersettings.module#UsersettingsPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: 'controlcenter/tabs/map',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'controlcenter/tabs/map',
        pathMatch: 'full'
    },
    { path: 'map', loadChildren: './map/map.module#MapPageModule' },
];
var TabsRoutingModule = /** @class */ (function () {
    function TabsRoutingModule() {
    }
    TabsRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], TabsRoutingModule);
    return TabsRoutingModule;
}());
export { TabsRoutingModule };
//# sourceMappingURL=tabs-routing.module.js.map