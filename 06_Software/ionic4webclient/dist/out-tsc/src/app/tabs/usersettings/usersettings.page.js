import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var UsersettingsPage = /** @class */ (function () {
    function UsersettingsPage() {
    }
    UsersettingsPage.prototype.ngOnInit = function () {
    };
    UsersettingsPage = tslib_1.__decorate([
        Component({
            selector: 'app-usersettings',
            templateUrl: './usersettings.page.html',
            styleUrls: ['./usersettings.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UsersettingsPage);
    return UsersettingsPage;
}());
export { UsersettingsPage };
//# sourceMappingURL=usersettings.page.js.map