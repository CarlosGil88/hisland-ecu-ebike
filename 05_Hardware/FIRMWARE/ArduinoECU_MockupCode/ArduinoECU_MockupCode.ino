/*
  Basic ESP8266 MQTT example
  This sketch demonstrates the capabilities of the pubsub library in combination
  with the ESP8266 board/library.
  It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off
  It will reconnect to the server if the connection is lost using a blocking
  reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
  achieve the same result without blocking the main loop.
  To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

  Library & Documentation API about PubSubClient() :
  https://pubsubclient.knolleary.net/api.html


*/

/*------------- LIBRARIES Includes & Defines --------------*/
#include <Adafruit_GPS.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(14, 12); //RX,TX
Adafruit_GPS GPS(&mySerial);
#define GPSECHO  true


/*------------- VARIABLES Declaration --------------*/
WiFiClient espClient;
PubSubClient client(espClient); //id

//wifi vars
const char* ssid = "Hisland";                           //"Your WiFi SSID";
const char* password = "W#tofigo82fa1He";               //"Your WiFi password";

//mqtt vars
const char* mqtt_server = "m24.cloudmqtt.com" ;         // at CloudMQTT web :"Your MQTT Server IP address";
const char* mqtt_user = "rxhwkrwu" ;//"cwwyilxh";                     // Your user at CloudMQTT
const char* mqtt_password = "euP2FEqY5IlV";//"ch3eicTMfASo";             //  Your password at CloudMQTT
const  int port = 14889 ;//12203;                                //just Port, not the SSL

//fo show Led vars
int LED = D2;   // WHITE LED - Light          // WeMos pin D2
int POWER = D1 ;// ORANGE LED                 // WeMos pin D8
int BAT = A0;   // reads battery High or Low  // WeMos pin A0
int BAT_RED = 0 ;// RED LED                   // WeMos pin D3
int BAT_GR = 2;  // GREEN LED                 // WeMos pin D4

//gps vars
char coord[60];
char data[40];
char latitude[20];
char longitude[20];
char altitude[20];

//batt vars
double battery = 90;
char battstring[2];

// sendLocationStatus and sendDiagnostic timers
uint32_t timer = millis();
uint32_t timerDiagnostics = millis();

//status and control vars of light and power
char* power = "false";
char* light = "false";

//diagnostic vars
char* breaks = "ok";
char* engine = "engine-paused health-ok";
char* ecu = "ecu-running-ok LiPo-bat-recharging";
char* batt = "f'healthd(0) battery l={batt_l} v={batt_v} t={batt_t} h=2 st=2'";


/*------------- FUNCTIONS Declaration --------------*/
void setup_wifi(void);
bool isTopic(char* topic, char* publishTopic);
char isPayload(byte* payload, char compchr);
void callback(char* topic, byte* payload, unsigned int length);
void errorMessage(void);
void batteryDebug(void);
void buzzer(void);
void setup_blink(void);
void sendLocationStatus(void);
void sendDiagnostics(void);
void subscribingMQTT();


/*--------------------- SET UP  ------------------------*/
void setup() {
  setup_blink();
  Serial.begin(115200);
  //delay(5000);
  Serial.setDebugOutput(true);
  setup_wifi();
  client.setServer(mqtt_server, port); // server, Port (not SSL)
  client.setCallback(callback);

  GPS.begin(9600);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA);
  delay(1000);
  // Ask for firmware version
  mySerial.println(PMTK_Q_RELEASE);
}

//fo show
void setup_blink() { // Make all the LEDs blink to be sure the connections are good and debugg
  pinMode(BUILTIN_LED, OUTPUT);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  delay(500);
  digitalWrite(LED, HIGH);
  pinMode(POWER, OUTPUT);
  digitalWrite(POWER, LOW);
  delay(500);
  digitalWrite(POWER, HIGH);
  pinMode(BAT, INPUT);
  pinMode(BAT_GR, OUTPUT);
  digitalWrite(BAT_GR, LOW);
  delay(500);
  digitalWrite(BAT_GR, HIGH);
  pinMode(BAT_RED, OUTPUT);
  digitalWrite(BAT_RED, LOW);
  delay(500);
  digitalWrite(BAT_RED, HIGH);
  buzzer();
}

void buzzer() { // blinks the orange LED
  digitalWrite(POWER, LOW);
  delay(100);
  digitalWrite(POWER, HIGH);
  delay(100);
  digitalWrite(POWER, LOW);
  delay(100);
  digitalWrite(POWER, HIGH);
  delay(100);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("MQTTEcuClient", mqtt_user, mqtt_password)) {
      Serial.println("connected");
      client.publish("outTopic", "hello world init");
      subscribingMQTT();
    } else {
      Serial.print("failed, client current state: ");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void subscribingMQTT() {
  client.subscribe("TestIn"); // for debugging
  client.subscribe("light/bool");
  client.subscribe("power/bool");
  client.subscribe("battery/call");
  client.subscribe("curstate/call");
}


//utility methods
bool isTopic(char* topic, char* publishTopic) {
  return (strcmp(topic, publishTopic) == 0);
}

char isPayload(byte* payload, char compchr) {
  return ((char)payload[0] == compchr);
}




void callback(char* topic, byte* payload, unsigned int length) {
  // go to CloudMQTT WEBSOCKET UI to send messages
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.println((char)payload[i]);
  }

  if (isTopic(topic, "light/bool")) {
    if (isPayload(payload, '0')) {
      digitalWrite(LED, HIGH);   // OFF LED
      light = "false";
      client.publish("ecu/debug/light/status", light);
    } else if (isPayload(payload, '1')) {
      digitalWrite(LED, LOW);  // ON LED
      light = "true";
      client.publish("ecu/debug/light/status", light);
    }

  } else if (isTopic(topic, "power/bool")) {
    if (isPayload(payload, '0')) {
      buzzer();
      digitalWrite(POWER, HIGH);  // OFF POWER
      power = "false";
      client.publish("ecu/debug/power/status", power);
    } else if (isPayload(payload, '1')) {
      digitalWrite(POWER, LOW);  // ON POWER
      buzzer();
      digitalWrite(POWER, LOW);  // ON POWER
      power = "true";
      client.publish("ecu/debug/power/status", power);
    }

  } else if (isTopic(topic, "TestIn")) { //DEBUG & Test
    if (isPayload(payload, 'b')) {
      batteryDebug();  // Read Battery Status
    } else if (isPayload(payload, 'k')) {
      setup_blink();  // for debug
    }

  } else if (isTopic(topic, "battery/call")) {
    client.publish("battery/json", "{\"id\":0,\"cycle\":\"85\",\"status\":\"46\" }");

  } else if (isTopic(topic, "curstate/call")) {
    dtostrf(GPS.latitude, 6, 2, latitude);
    dtostrf(GPS.longitude, 6, 2, longitude);
    dtostrf(GPS.altitude, 6, 2, altitude);
    dtostrf(battery, 2, 0, battstring);

    sprintf(data, "{\"id\":0,\"lat\":%s%s,\"lng\":%s%s,\"alt\":%s,\"bat\":%s}", &GPS.lat, latitude, &GPS.lon, longitude, altitude, battstring); //98 is a random num for batt %
    client.publish("curstate/json", data);

  } else {
    //errorMessage
    client.publish("inTopic/error", "error: topic doesn't match any callback topic");
  }
}





void batteryDebug() { //It reads the pin A0- connect the wire to 5V or 3.3V (will be High status)or GND(will be Low status)
  int batValue = 0;
  batValue = analogRead(BAT);
  digitalWrite(BAT_GR, HIGH);
  digitalWrite(BAT_RED, HIGH);
  if ( batValue < 20 ) {
    Serial.print("Publish message: ");
    Serial.println(batValue);
    client.publish("debug/batteryDebug", "Low");
    digitalWrite(BAT_RED, LOW);
  } else if ( batValue > 150 ) {
    Serial.print("Publish message: ");
    Serial.println(batValue);
    client.publish("debug/batteryDebug", "High");
    digitalWrite(BAT_GR, LOW);
  } else {
    client.publish("debug/batteryDebug", "Normal");
    digitalWrite(BAT_GR, HIGH);
    digitalWrite(BAT_RED, HIGH);
  }
}


/*****************************************************************************/
/***************************    LOOP    **************************************/
/*****************************************************************************/
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  sendLocationStatus();
  sendDiagnostics();
  sendBatteryStatus();
}

//loopy bits
void sendDiagnostics() {
  if (timerDiagnostics > millis())  timerDiagnostics = millis();
  if (millis() - timerDiagnostics > 2500) {
    timerDiagnostics = millis(); // reset the
    Serial.println("Diagnostics: ");
    //Serial.println("{\"id\":0,\"battery\":\"ok\",\"wheels\":\"ok\", \"lights\": [{\"front\": \"ok\"},{\"back\": \"ok\"},{\"other\": \"ok\"} ], \"suspention\": \"ok\", \"breaks\": \"ok\" }");
    //client.publish("Diagnostics","{\"id\":0,\"battery\":\"ok\",\"breaks\":\"ok\", \"lights\"[{\"front\": \"ok\"},{\"back\": \"ok\"},{\"suspention\": \"ok\"} ]");
    client.publish("diagnostics/breaks/string", breaks);
    client.publish("diagnostics/engine/string", engine);
    client.publish("diagnostics/ecu/string", ecu);
    client.publish("diagnostics/battery/string", batt);
  }
}

void sendLocationStatus() {
  char c = GPS.read();
  if ((c) && (GPSECHO))
    Serial.write(c);
  if (GPS.newNMEAreceived()) {
    if (!GPS.parse(GPS.lastNMEA()))
      return;
  }
  if (timer > millis())  timer = millis();
  if (millis() - timer > 2000) {
    timer = millis(); // reset the timer
    if (GPS.fix) {
      Serial.print("Location: ");
      Serial.print(GPS.latitude, 4); Serial.print(GPS.lat);
      Serial.print(", ");
      Serial.print(GPS.longitude, 4); Serial.println(GPS.lon);

      Serial.print("Speed (knots): "); Serial.println(GPS.speed);
      Serial.print("Angle: "); Serial.println(GPS.angle);
      Serial.print("Altitude: "); Serial.println(GPS.altitude);
      Serial.print("Satellites: "); Serial.println((int)GPS.satellites);
    }

    dtostrf(GPS.latitude, 6, 2, latitude);
    dtostrf(GPS.longitude, 6, 2, longitude);
    sprintf (coord, "{\"id\":0,\"longitude\":%s,\"latitude\":%s}", latitude, longitude);
    Serial.print("Publish message: ");
    Serial.println(coord);
    client.publish("gps/coordinates", coord);
  }
}


void sendBatteryStatus(){
  if (timer > millis())  timer = millis();
  if (millis() - timer > 8000) {
    timer = millis(); // reset the timer
    dtostrf(battery, 2, 0, battstring);
    client.publish("battery/json", battstring);
    battery = battery - 1;
  }
}









